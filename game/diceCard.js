'use strict';

const Card = require('./Card.js');

const diceCardClass = class diceCard extends Card {
    constructor(name, value) {
        super(name, 'dice', value);
    }
}

module.exports = diceCardClass;
