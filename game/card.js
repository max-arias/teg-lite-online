'use strict';

const uniqid = require('uniqid');

const cardClass = class Card {
    constructor(name, type, value) {
        this.id = uniqid();
        this.name = name;
        this.type = type;
        this.value = value;
        this.inPlay = false;
        this.used   = false;
    }

    someFunction() {
        return 'some output';
    }
}

module.exports = cardClass;
