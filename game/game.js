'use strict';

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const DiceCard = require('./diceCard.js');

const gameClass = class Game {    
    constructor(io, players) {
        this.io = io;
        this.players = players;
        this.playerCount = Object.keys(players).length;

        this.diceDeck = [];

        this.generateDiceCards();
        this.distributeCards();
        this.notifyPlayers();
        this.listenForActions();
    }

    generateDiceCards() {

        const loadedDice = yaml.safeLoad(fs.readFileSync(path.resolve(__dirname, 'cards/dice.yml'), 'utf8'));
        loadedDice.die.map((dieVal) => {
            const dice = new DiceCard(`card-${dieVal}`, dieVal);
            this.diceDeck.push(dice);
        });
    }

    distributeCards() {
        const shuffledCards = _.shuffle(this.diceDeck);
        this.chunkedDiceDeck = _.chunk(shuffledCards, shuffledCards.length / this.playerCount);

        let iter = 0;
        _.each(this.players, (player, key) => {
            player.diceDeck = _.keyBy(this.chunkedDiceDeck[iter], 'id');
            iter++;
        });
    }

    notifyPlayers() {
        _.each(this.players, (player, key) => {
            this.io.to(key).emit('initialCards', { 'dice': player.diceDeck });
        });
    }
    
    updateDice(id, action) {
        // switch (action) {
        //     case 'inPlay':
                
        //     break;
        //     case 'played':
        //     break;
        // }
    }

    listenForActions() {
        const that = this;
        _.each(this.players, (player, key) => {
            player.socket.on('diceUsed', (diceUsed) => {
                that.updateDice(diceUsed, 'inPlay')
            });
        });
    }

 }

module.exports = gameClass;
