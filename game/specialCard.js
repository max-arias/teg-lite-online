'use strict';

const Card = require('./Card.js');

const specialCardClass = class specialCard extends Card {
    constructor() {
        super('special card name', 'special', {nothing: 'here'});
    }
}

module.exports = specialCardClass;
