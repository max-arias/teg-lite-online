const socket = require('socket.io');
const _ = require('lodash');
const Game = require('./game/game');
const io = socket();
const socketApi = {};
const connections = {};

socketApi.io = io;

//TODO: This should be variable per game
const MAX_PLAYERS = 2;

const getAllPlayers = () => _.map(connections, (player) => {
    return {
        id: player.id,
        name: player.name,
    }
});

const returnAllPlayers = () => {
    const allPlayers = getAllPlayers();
    io.sockets.emit('allPlayers', allPlayers);
};

// Start the game
const initGame = () => {
    const game = new Game(io, connections);
};

io.on('connection', function (socket) {
    console.log("Connected to Socket: "+ socket.id)	
    connections[socket.id] = {
        socket: socket
    };
    
    socket.on('disconnect', function(){
        console.log('Disconnected - '+ socket.id);
        delete connections[socket.id];

        // Notify everyone of players
        returnAllPlayers();
    });

    socket.on('playerjoin', (player) => {
        // Basic player object
        connections[socket.id] = {
            id: player.id,
            name: player.name,
            ready: false,
            socket: socket
        }

        // Notify everyone of players
        returnAllPlayers();
    });

    if (Object.keys(connections).length === MAX_PLAYERS) {
        initGame();
    }

    //TODO: We should wait for this from the loby
    socket.on('allPlayersReady', initGame);
});

module.exports = socketApi;