import _ from 'lodash';

const initialState = {
  dice: {},
  specials: {},
  countries: {},
  players: [],
  objective: {},
  waitingForResponse: false,
  yourTurn: false
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_PLAYERS':
      return {
        ...state,
        players: action.payload
      }
    case 'SET_INITIAL_CARDS':
      return {
        ...state,
        ...action.payload
      }
    case 'DICE_USED':
      return {
        ...state,
        dice: {
          ...state.dice,
          [action.payload]: {
            ...state.dice[action.payload],
            inPlay: true
          }
        }
      }
    default:
      return state
  }
}

export default reducer