import io from 'socket.io-client';
import uniqid from 'uniqid';
import React, { Component } from 'react';
import {connect} from 'react-redux'
import {
  socketLoadInitialConnection,
  socketSetCurrentPlayers,
  socketSetInitialCards,
  socketUseDiceCard,
} from './actions/action'

import './App.css';

let socket;

const mapStateToProps = (state) => {
  return {...state};
};

class App extends Component {
  state = {users: []}

  constructor(props)
  {
    super(props)
    const { dispatch } = props

    socket = io.connect("http://localhost:3001")

    const currentPlayer = {id: uniqid() , 'name': `test-${uniqid()}`};

    dispatch(socketLoadInitialConnection(socket, currentPlayer));

    socket.on('allPlayers', function(players) {
      dispatch(socketSetCurrentPlayers(players))
    });

    socket.on('initialCards', function(initialCards) {
      dispatch(socketSetInitialCards(initialCards))
    });
  }

  componentWillUnmount() {
    socket.disconnect();
  }

  useDice(id) {
    const { dispatch } = this.props
    dispatch(socketUseDiceCard(socket, id))
  }

  generateDiceCards() {
    const { dice } = this.props;

    return Object.keys(dice).map(key => 
      (
        <div key={key} className="die" onClick={() => {
          this.useDice(key);
        }}>
          <span>{dice[key].value}</span>
        </div>
      )
    );
  }

  render() {
    const { players } = this.props;
    return (
      <div className="App">
        <ul className="players">
          {players.map((player, index) => {
            return (
              <li key={index}>
                <img src={`https://api.adorable.io/avatars/80/${player.id}.png`} alt={player.name}/>
                <span>ID: {player.id}</span>
                <span>Name: {player.name}</span>
              </li>
            )
          })}
        </ul>
        <hr/>
        <div className="cards">
          <div className="dice">
            {this.generateDiceCards()}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(App)