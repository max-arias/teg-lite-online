export const setPlayers = (players) => ({
	type: "SET_PLAYERS",
	payload: players
})

export const setInitialCards = (initialCards) => ({
	type: "SET_INITIAL_CARDS",
	payload: initialCards
})

export const diceUsed = (diceId) => ({
	type: "DICE_USED",
	payload: diceId
})

export const socketLoadInitialConnection = (socket, currentPlayer) => {
	return (dispatch) => {
		socket.emit('playerjoin', currentPlayer);
		dispatch(setPlayers([currentPlayer]));
	}
}

export const socketSetCurrentPlayers = (players) => {
	return (dispatch) => {
		dispatch(setPlayers(players));
	}
}

export const socketSetInitialCards = (gameData) => {
	return (dispatch) => {
		dispatch(setInitialCards(gameData));
	}
}

export const socketUseDiceCard = (socket, diceId) => {
	return (dispatch) => {
		socket.emit('diceUsed', diceId);
		dispatch(diceUsed(diceId));
	}
}
